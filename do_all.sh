#!/bin/sh

cd $(dirname $0)

# working
./update-pkg set git git://github.com/trustedsec/social-engineer-toolkit.git
./update-pkg aircrack-ng svn http://svn.aircrack-ng.org/trunk/
./update-pkg dnsrecon git git://github.com/darkoperator/dnsrecon.git
./update-pkg sqlmap git git://github.com/sqlmapproject/sqlmap.git 
./update-pkg rfidiot git git://github.com/AdamLaurie/RFIDIOt.git
./update-pkg beef-xss git git://github.com/beefproject/beef.git
./update-pkg libnfc git https://code.google.com/p/libnfc/
./update-pkg libfreefare git https://code.google.com/p/libfreefare/
./update-pkg mfoc git https://code.google.com/p/mfoc/
./update-pkg mfcuk svn http://mfcuk.googlecode.com/svn/trunk/
./update-pkg johnny git git://github.com/AlekseyCherepanov/johnny.git

# not looked at yet
#./update-pkg airoscript-ng svn http://svn.aircrack-ng.org/branch/airoscript-ng/ 
#./update-pkg wpscan git git://github.com/wpscanteam/wpscan.git
#./update-pkg powersploit git git://github.com/mattifestation/PowerSploit.git
#./update-pkg exploitdb svn svn://svn.exploit-db.com/exploitdb

